// Fill out your copyright notice in the Description page of Project Settings.


#include "AnimInstanceDefault.h"

void UAnimInstanceDefault::PlayAnim(UAnimMontage* InAnimBP, bool& bOut)
{
	if (InAnimBP)
	{
		bOut = true;
		Montage_Play(InAnimBP);
	}
	else
	{
		bOut = false;
	}
}
