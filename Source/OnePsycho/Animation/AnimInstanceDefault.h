// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "AnimInstanceDefault.generated.h"

/**
 * 
 */
UCLASS(Blueprintable)
class ONEPSYCHO_API UAnimInstanceDefault : public UAnimInstance
{
	GENERATED_BODY()

		UFUNCTION(BlueprintCallable, Category = "Default|Montage")
		void PlayAnim(UAnimMontage* InAnimBP, bool& bOut);
};
