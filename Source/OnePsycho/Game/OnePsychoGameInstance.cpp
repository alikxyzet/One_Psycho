// Fill out your copyright notice in the Description page of Project Settings.


#include "OnePsychoGameInstance.h"

bool UOnePsychoGameInstance::GetWeaponInfoByName(EWeaponType WeaponType, FWeaponInfo& OutInfo)
{
	bool bIsFind = false;
	FWeaponInfo* WeaponInfoRow;
	
	WeaponInfoRow = WeaponInfoTable->FindRow<FWeaponInfo>(EWeaponName[(uint8)WeaponType], "", false);
	if (WeaponInfoRow)
	{
		bIsFind = true;
		OutInfo = *WeaponInfoRow;
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("UOnePsychoGameInstance::GetWeaponInfoByName - WeaponTable -NULL"));

	return bIsFind;
}

bool UOnePsychoGameInstance::GetDropItemInfoByName(EWeaponType WeaponType, FDropItem& OutInfo)
{
	bool bIsFind = false;

	if (DropItemInfoTable)
	{
		FDropItem* DropItemInfoRow;
		TArray<FName>RowNames = DropItemInfoTable->GetRowNames();

		int8 i = 0;
		while (i < RowNames.Num() && !bIsFind)
		{
			DropItemInfoRow = DropItemInfoTable->FindRow<FDropItem>(RowNames[i], "");
			if (DropItemInfoRow->WeaponInfo.WeaponType == WeaponType)
			{
				OutInfo = (*DropItemInfoRow);
				bIsFind = true;
			}
			i++;//fix
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UOnePsychoGameInstance::GetDropItemInfoByName - DropItemInfoTable -NULL"));
	}

	return bIsFind;
}

bool UOnePsychoGameInstance::GetDropItemInfoByWeaponName(EWeaponType WeaponType, FDropItem& OutInfo)
{
	bool bIsFind = false;

	if (DropItemInfoTable)
	{
		FDropItem* DropItemInfoRow;
		TArray<FName>RowNames = DropItemInfoTable->GetRowNames();

		int8 i = 0;
		while (i < RowNames.Num() && !bIsFind)
		{
			DropItemInfoRow = DropItemInfoTable->FindRow<FDropItem>(RowNames[i], "");
			if (DropItemInfoRow->WeaponInfo.WeaponType == WeaponType)
			{
				OutInfo = (*DropItemInfoRow);
				bIsFind = true;
			}
			i++;//fix
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UOnePsychoGameInstance::GetDropItemInfoByName - DropItemInfoTable -NULL"));
	}

	return bIsFind;
}