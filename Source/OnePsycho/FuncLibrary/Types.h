﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"

#include "Math/TransformNonVectorized.h"
#include <string>

#include "OnePsycho/StateEffects/OP_StateEffect.h"

#include "Types.generated.h"



UCLASS(BlueprintType)
class ONEPSYCHO_API UTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	// ---   PID-controller for float   ---
	// PID for C++
	static void PIDController(
		float Setpoint,
		float Feedback,
		float& Output,

		float ProportionalCoeff,

		float Clamp = 0,

		bool* bNearlyEqual = nullptr,
		float DeviationTolerance = 0,
		int NumberOfChecks = 0,
		int* OutputChecksNumber = nullptr,

		float DeltaTime = 0,
		float* IntegralInput = nullptr,
		float IntegralCoeff = 0,

		float* PreviousErrorValueInput = nullptr,
		float DerivativeCoeff = 0);

	// PController for Blueprint
	UFUNCTION(BlueprintCallable, meta = (AdvancedDisplay = "4"))
	static void PControllerGlobal(
		float Setpoint,
		float Feedback,
		float& Output,
		float ProportionalCoeff,

		float DeviationTolerance,
		float Clamp,
		int NumberOfChecks,
		bool& bNearlyEqual,
		int& OutputChecksNumber);

	// PIController for Blueprint
	UFUNCTION(BlueprintCallable, meta = (AdvancedDisplay = "6"))
	static void PIControllerGlobal(
		float Setpoint,
		float Feedback,
		float& Output,
		float DeltaTime,
		float ProportionalCoeff,
		float IntegralCoeff,

		float DeviationTolerance,
		float Clamp,
		int NumberOfChecks,
		bool& bNearlyEqual,
		float& IntegralValue,
		int& OutputChecksNumber);

	// PIDController for Blueprint
	UFUNCTION(BlueprintCallable, meta = (AdvancedDisplay = "7"))
	static void PIDControllerGlobal(
		float Setpoint,
		float Feedback,
		float& Output,
		float DeltaTime,
		float ProportionalCoeff,
		float IntegralCoeff,
		float DerivativeCoeff,

		float DeviationTolerance,
		float Clamp,
		int NumberOfChecks,
		bool& bNearlyEqual,
		float& IntegralValue,
		float& PreviousErrorValue,
		int& OutputChecksNumber);
	// ----------------------------------------------------------------------------------------------------



	// ---   PID-controller for FVector   ---
	// PID for C++
	static void PIDController(
		FVector Setpoint,
		FVector Feedback,
		FVector& Output,

		float ProportionalCoeff,

		float Clamp = 0,

		bool* bNearlyEqual = nullptr,
		float DeviationTolerance = 0,
		int NumberOfChecks = 0,
		int* OutputChecksNumber = nullptr,

		float DeltaTime = 0,
		FVector* IntegralInput = nullptr,
		float IntegralCoeff = 0,

		FVector* PreviousErrorValueInput = nullptr,
		float DerivativeCoeff = 0);

	// PController for Blueprint
	UFUNCTION(BlueprintCallable, meta = (AdvancedDisplay = "4"))
	static void PControllerGlobalForVector(
		FVector Setpoint,
		FVector Feedback,
		FVector& Output,
		float ProportionalCoeff,

		float DeviationTolerance,
		float Clamp,
		int NumberOfChecks,
		bool& bNearlyEqual,
		int& OutputChecksNumber);

	// PIController for Blueprint
	UFUNCTION(BlueprintCallable, meta = (AdvancedDisplay = "6"))
	static void PIControllerGlobalForVector(
		FVector Setpoint,
		FVector Feedback,
		FVector& Output,
		float DeltaTime,
		float ProportionalCoeff,
		float IntegralCoeff,

		float DeviationTolerance,
		float Clamp,
		int NumberOfChecks,
		bool& bNearlyEqual,
		FVector& IntegralValue,
		int& OutputChecksNumber);

	// PIDController for Blueprint
	UFUNCTION(BlueprintCallable, meta = (AdvancedDisplay = "7"))
	static void PIDControllerGlobalForVector(
		FVector Setpoint,
		FVector Feedback,
		FVector& Output,
		float DeltaTime,
		float ProportionalCoeff,
		float IntegralCoeff,
		float DerivativeCoeff,

		float DeviationTolerance,
		float Clamp,
		int NumberOfChecks,
		bool& bNearlyEqual,
		FVector& IntegralValue,
		FVector& PreviousErrorValue,
		int& OutputChecksNumber);
	// ----------------------------------------------------------------------------------------------------



	/* ---   Interface   --- */
	UFUNCTION(BlueprintCallable)
	static void AddEffectBySurfaceType(
		AActor* TakeEffectActor,
		FName NameBoneHit,
		TSubclassOf<UOP_StateEffect> AddEffectClass,
		EPhysicalSurface SurfaceType);
	// ----------------------------------------------------------------------------------------------------



	/* ---   Test   --- */
	//UFUNCTION(BlueprintCallable)
	//	static void TestType(
	//		int Input,
	//		int& Output);
	// ----------------------------------------------------------------------------------------------------
};



/* ---   Movement   --- */
// Виды статуса передвижения
UENUM(BlueprintType)
enum struct EMovementState : uint8
{
	Aim_State		UMETA(DisplayName = "Aim State"),
	AimWalk_State	UMETA(DisplayName = "AimWalk State"),
	Walk_State		UMETA(DisplayName = "Walk State"),
	Run_State		UMETA(DisplayName = "Run State"),
	SprintRun_State UMETA(DisplayName = "SprintRun State")
};

// Скорость персонажа 
USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float AimSpeedNormal = 300.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float WalkSpeedNormal = 200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float RunSpeedNormal = 600.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float AimSpeedWalk = 100.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
	float SprintRunSpeedRun = 800.0f;

};
// ----------------------------------------------------------------------------------------------------



/* ---   Doors   --- */
// Виды статуса дверей
UENUM(BlueprintType)
enum struct EDoorState : uint8
{
	IsOpen_State	UMETA(DisplayName = "Is Open State"),
	IsClose_State	UMETA(DisplayName = "Is Close State"),
	OnlyOpen_State	UMETA(DisplayName = "Only Open State"),
	OnlyClose_State UMETA(DisplayName = "Only Close State"),
	OpenClose_State UMETA(DisplayName = "Open-Close State")
};
// ----------------------------------------------------------------------------------------------------



/* ---   Projectile   --- */
// Тип стрельбы
UENUM(BlueprintType)
enum struct ETypeFire : uint8
{
	//Auto UMETA(DisplayName = "Automatic selection"),
	Ballistics	UMETA(DisplayName = "Ballistics"),
	HitScan		UMETA(DisplayName = "HitScan")
};

// Основные параматры снаряда/пули
USTRUCT(BlueprintType)
struct FProjectileInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	ETypeFire TypeFire = ETypeFire::Ballistics;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting", meta = (EditCondition = "TypeFire == ETypeFire::Ballistics"))
	TSubclassOf<class AProjectileDefault> Projectile = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting", meta = (EditCondition = "TypeFire == ETypeFire::Ballistics"))
	UStaticMesh* ProjectileStaticMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting", meta = (EditCondition = "TypeFire == ETypeFire::Ballistics"))
	FTransform ProjectileStaticMeshOffset = FTransform();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting", meta = (EditCondition = "TypeFire == ETypeFire::Ballistics"))
	UParticleSystem* ProjectileTrailFX = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting", meta = (EditCondition = "TypeFire == ETypeFire::Ballistics"))
	FTransform ProjectileTrailFXOffset = FTransform();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting", meta = (EditCondition = "TypeFire == ETypeFire::Ballistics"))
	float ProjectileInitSpeed = 2000.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	float ProjectileMaxSpeed = 2000.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	float ProjectileLifeTime = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ProjectileSetting")
	float ProjectileDamage = 20.0f;



	/* ---   FX   --- */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
	TMap<TEnumAsByte<EPhysicalSurface>, UMaterialInterface*> HitDecals;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
	TMap<TEnumAsByte<EPhysicalSurface>, USoundBase*> HitSound;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
	TMap<TEnumAsByte<EPhysicalSurface>, UParticleSystem*> HitFXs;
	// ----------------------------------------------------------------------------------------------------

	/* ---   Explode   --- */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
	UParticleSystem* ExplodeFX = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
	USoundBase* ExplodeSound = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
	float ProjectileMaxRadiusDamage = 200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
	float ProjectileMinRadiusDamage = 200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
	float ExplodeMaxDamage = 40.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
	float ExplodeFalloffCoef = 1.f;
	// ----------------------------------------------------------------------------------------------------

	/* ---   Interface   --- */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effect")
	TSubclassOf<UOP_StateEffect> Effect = nullptr;
	// ----------------------------------------------------------------------------------------------------
};
// ----------------------------------------------------------------------------------------------------



/* ---   Dispersion   --- */
USTRUCT(BlueprintType)
struct FDispersionInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float AimMax = 2.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float AimMin = 0.3f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float Recoil = 1.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	float Reduction = 0.3f;
};
// ----------------------------------------------------------------------------------------------------



/* ---   Drop Mesh   --- */

USTRUCT(BlueprintType)
struct FDropImpulse
{
	GENERATED_BODY()

protected:

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FVector Location;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FRotator Direction;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	FRotator Rotation;

public:

	FDropImpulse()
	{
		Location = FVector(0);
		Direction = FRotator(0);
		Rotation = FRotator(0);
	};

	// Делегирующий
	FDropImpulse(const FTransform& InTransform) : FDropImpulse()
	{
		SetTransform(InTransform);
	};

	// Делегирующий
	FDropImpulse(const FTransform& InTransform, const FRotator& InRotator) : FDropImpulse(InTransform)
	{
		SetRotation(InRotator);
	};



	/* ---   Location   --- */
	const FVector GetLocation() const
	{
		return Location;
	}

	void SetLocation(const FVector& InLocation)
	{
		Location = InLocation;
	}
	// ----------------------------------------------------------------------------------------------------



	/* ---   Direction   --- */
	const FRotator GetDirection() const
	{
		return Direction;
	}

	void AddDirection(const FRotator& InDirection)
	{
		Direction += InDirection;
	}

	const FVector GetDirectionNormalize(const float& Tolerance) 
	{
		return Direction.Vector() * Tolerance;
	}
	// ----------------------------------------------------------------------------------------------------



	/* ---   Rotation   --- */
	const FRotator GetRotation() const
	{
		return Rotation;
	}

	void SetRotation(const FRotator& InRotation)
	{
		Rotation = InRotation;
	}
	// ----------------------------------------------------------------------------------------------------



	/* ---   Transform   --- */
	const FTransform GetTransform() const
	{
		FTransform out_Transform;
		out_Transform.SetLocation(Location);
		out_Transform.SetRotation(FQuat(Direction));
		return out_Transform;
	}

	void SetTransform(const FTransform& InTransform)
	{
		Location = InTransform.GetLocation();
		Direction = FRotator(InTransform.GetRotation());
	}
	// ----------------------------------------------------------------------------------------------------
};



USTRUCT(BlueprintType)
struct FDropMeshInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
	UStaticMesh* DropMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh", meta = (EditCondition = "DropMesh != nullptr"))
	float DropMeshTime = 0.2f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh", meta = (EditCondition = "DropMesh != nullptr"))
	float DropMeshLifeTime = 60.f;

	// Custom or Default
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh", meta = (EditCondition = "DropMesh != nullptr"))
	bool bCustomDropImpulse = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh", meta = (EditCondition = "bCustomDropImpulse && DropMesh != nullptr"))
	FDropImpulse DropMeshImpulse;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh", meta = (EditCondition = "DropMesh != nullptr"))
	float PowerImpulse = 100.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh", meta = (EditCondition = "DropMesh != nullptr"))
	float ImpulseRandomDispersion = 90.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh", meta = (EditCondition = "DropMesh != nullptr"))
	float CustomMass = 0.f;
};
// ----------------------------------------------------------------------------------------------------



/* ---   Animations   --- */
USTRUCT(BlueprintType)
struct FWeaponAnim
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
	UAnimMontage* Fire = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
	UAnimMontage* ReloadFromHip = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
	UAnimMontage* ReloadWhenAiming = nullptr;
};

USTRUCT(BlueprintType)
struct FCharacterAnim : public FWeaponAnim
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
	UAnimMontage* BasicAnimation = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
	UAnimMontage* BasicAiming = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
	UAnimMontage* AimedFire = nullptr;
};
// ----------------------------------------------------------------------------------------------------



/* ---   Weapon   --- */
static const char* EWeaponName[] =
{
	"Rifle_v1",
	"SniperRifle_v1",
	"Pistol_v1",
	"GrenadeLauncher_v1",
	"Shotgun_v1",
	"RocketLauncher_v1",
	"Test_v1"
};

UENUM(BlueprintType)
enum struct EWeaponType : uint8
{
	Rifle_v1			UMETA(DisplayName = "Rifle_v1"),
	SniperRifle_v1		UMETA(DisplayName = "SniperRifle_v1"),
	Pistol_v1			UMETA(DisplayName = "Pistol_v1"),
	GrenadeLauncher_v1	UMETA(DisplayName = "GrenadeLauncher_v1"),
	Shotgun_v1			UMETA(DisplayName = "Shotgun_v1"),
	RocketLauncher_v1	UMETA(DisplayName = "RocketLauncher_v1"),
	Test_v1				UMETA(DisplayName = "Test_v1"),
};

USTRUCT(BlueprintType)
struct FWeaponDispersion
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion|Aim State")
	FDispersionInfo Aim_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion|AimWalk State")
	FDispersionInfo AimWalk_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion|Walk State")
	FDispersionInfo Walk_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion|Run State")
	FDispersionInfo Run_State;
};

USTRUCT(BlueprintType)
struct FWeaponInfo : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Class")
	TSubclassOf<class AWeaponDefault> WeaponClass = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
	float RateOfFire = 0.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
	float ReloadTime = 2.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
	int32 MaxRound = 10;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
	int32 NumberProjectileByShot = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
	FWeaponDispersion DispersionWeapon;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
	USoundBase* SoundFireWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
	USoundBase* SoundReloadWeapon = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
	UParticleSystem* EffectFireWeapon = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
	FProjectileInfo ProjectileSetting;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace")
	float WeaponDamage = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace")
	float DistanceTrace = 2000.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitEffect")
	UDecalComponent* DecalOnHit = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
	FCharacterAnim CharacterAnim;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
	FWeaponAnim WeaponAnim;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
	FDropMeshInfo ClipDropMesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
	FDropMeshInfo SleeveBullets;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	float SwitchTimeToWeapon = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	UTexture2D* WeaponIcon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
	EWeaponType WeaponType = EWeaponType::Rifle_v1;

};

USTRUCT(BlueprintType)
struct FAdditionalWeaponInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Stats")
	int32 Round = 10;
};
// ----------------------------------------------------------------------------------------------------



/* ---   Slots   --- */
USTRUCT(BlueprintType)
struct FWeaponSlot
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSlot")
	bool bIsFreeSlot = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSlot")
	EWeaponType WeaponType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSlot")
	FAdditionalWeaponInfo AdditionalInfo;
};

USTRUCT(BlueprintType)
struct FAmmoSlot
{
	GENERATED_BODY()

	///Index Slot by Index Array
	/// Индекс слота по индексу массива
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
	EWeaponType WeaponType = EWeaponType::Rifle_v1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
	int32 Cout = 100;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
	int32 MaxCout = 100;
};
// ----------------------------------------------------------------------------------------------------



/* ---   Drop Item: Weapon   --- */
USTRUCT(BlueprintType)
struct FDropItem : public FTableRowBase
{
	GENERATED_BODY()

	///Index Slot by Index Array
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
	UStaticMesh* WeaponStaticMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
	USkeletalMesh* WeaponSkeletMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
	UParticleSystem* ParticleItem = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
	FTransform Offset;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
	FWeaponSlot WeaponInfo;
};
// ----------------------------------------------------------------------------------------------------



/* ---   Interface   --- */
class UOP_IGameActor;

// ----------------------------------------------------------------------------------------------------



/* ---   Demo   --- */

//----------------------------------------------------------------------------------------------------