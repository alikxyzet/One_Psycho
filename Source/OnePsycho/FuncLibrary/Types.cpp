﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "Types.h"
#include "OnePsycho/OnePsycho.h"
#include "OnePsycho/Interface/OP_IGameActor.h"


// ---   PID-controller for float   ---
void UTypes::PIDController(
	float Setpoint,
	float Feedback,
	float& Output,

	float ProportionalCoeff,

	float Clamp,

	bool* bNearlyEqual,
	float DeviationTolerance,
	int NumberOfChecks,
	int* OutputChecksNumber,

	float DeltaTime,
	float* IntegralInput,
	float IntegralCoeff,

	float* PreviousErrorValueInput,
	float DerivativeCoeff)
{
	float ErrorValue = Setpoint - Feedback;

	Output = ErrorValue * ProportionalCoeff;

	if (IntegralCoeff != 0)
	{
		(*IntegralInput) += ErrorValue * IntegralCoeff * DeltaTime;
		Output += *IntegralInput;
	}

	if (DerivativeCoeff != 0)
	{
		float Derivative = ((ErrorValue - *PreviousErrorValueInput) / DeltaTime) * DerivativeCoeff;
		Output += Derivative;
		*PreviousErrorValueInput = ErrorValue;
	}

	// Проверка стабилизации
	if (bNearlyEqual) {
		*bNearlyEqual = false;

		if (-DeviationTolerance <= ErrorValue && ErrorValue <= DeviationTolerance)
		{
			if (!OutputChecksNumber || *OutputChecksNumber >= NumberOfChecks)
			{
				*bNearlyEqual = true;
				if (IntegralInput)
					*IntegralInput = 0;
				if (PreviousErrorValueInput)
					*PreviousErrorValueInput = 0;
			}
			else
				if (OutputChecksNumber)
					(*OutputChecksNumber)++;
		}
		else
			if (OutputChecksNumber)
				*OutputChecksNumber = 0;
	}

	// Ограничение значения
	if (Clamp != 0)
		if (Output < -Clamp)
			Output = -Clamp;
		else if (Output > Clamp)
			Output = Clamp;
}

void UTypes::PControllerGlobal(
	float Setpoint,
	float Feedback,
	float& Output,
	float ProportionalCoeff,
	float DeviationTolerance,
	float Clamp,
	int NumberOfChecks,
	bool& bNearlyEqual,
	int& OutputChecksNumber)
{
	PIDController(
		Setpoint,
		Feedback,
		Output,
		ProportionalCoeff,
		Clamp,
		&bNearlyEqual,
		DeviationTolerance,
		NumberOfChecks,
		&OutputChecksNumber);
}

void UTypes::PIControllerGlobal(
	float Setpoint,
	float Feedback,
	float& Output,
	float DeltaTime,
	float ProportionalCoeff,
	float IntegralCoeff,
	float DeviationTolerance,
	float Clamp,
	int NumberOfChecks,
	bool& bNearlyEqual,
	float& IntegralValue,
	int& OutputChecksNumber)
{
	PIDController(
		Setpoint,
		Feedback,
		Output,
		ProportionalCoeff,
		Clamp,
		&bNearlyEqual,
		DeviationTolerance,
		NumberOfChecks,
		&OutputChecksNumber,
		DeltaTime,
		&IntegralValue,
		IntegralCoeff);
}

void UTypes::PIDControllerGlobal(
	float Setpoint,
	float Feedback,
	float& Output,
	float DeltaTime,
	float ProportionalCoeff,
	float IntegralCoeff,
	float DerivativeCoeff,
	float DeviationTolerance,
	float Clamp,
	int NumberOfChecks,
	bool& bNearlyEqual,
	float& IntegralValue,
	float& PreviousErrorValue,
	int& OutputChecksNumber)
{
	PIDController(
		Setpoint,
		Feedback,
		Output,
		ProportionalCoeff,
		Clamp,
		&bNearlyEqual,
		DeviationTolerance,
		NumberOfChecks,
		&OutputChecksNumber,
		DeltaTime,
		&IntegralValue,
		IntegralCoeff,
		&PreviousErrorValue,
		DerivativeCoeff);
}
// ----------------------------------------------------------------------------------------------------



// ---   PID-controller for FVector   ---
void UTypes::PIDController(
	FVector Setpoint,
	FVector Feedback,
	FVector& Output,

	float ProportionalCoeff,

	float Clamp,

	bool* bNearlyEqual,
	float DeviationTolerance,
	int NumberOfChecks,
	int* OutputChecksNumber,

	float DeltaTime,
	FVector* IntegralInput,
	float IntegralCoeff,

	FVector* PreviousErrorValueInput,
	float DerivativeCoeff)
{
	FVector ErrorValue = Setpoint - Feedback;

	Output = ErrorValue * ProportionalCoeff;

	if (IntegralCoeff != 0)
	{
		(*IntegralInput) += ErrorValue * IntegralCoeff * DeltaTime;
		Output += *IntegralInput;
	}

	if (DerivativeCoeff != 0)
	{
		FVector Derivative = ((ErrorValue - *PreviousErrorValueInput) / DeltaTime) * DerivativeCoeff;
		Output += Derivative;
		*PreviousErrorValueInput = ErrorValue;
	}

	// Проверка стабилизации
	if (bNearlyEqual) {
		*bNearlyEqual = false;

		if (-DeviationTolerance <= ErrorValue.Size() && ErrorValue.Size() <= DeviationTolerance)
		{
			if (!OutputChecksNumber || *OutputChecksNumber >= NumberOfChecks)
			{
				*bNearlyEqual = true;
				if (IntegralInput)
					*IntegralInput = FVector(0);
				if (PreviousErrorValueInput)
					*PreviousErrorValueInput = FVector(0);
			}
			else
				if (OutputChecksNumber)
					(*OutputChecksNumber)++;
		}
		else
			if (OutputChecksNumber)
				*OutputChecksNumber = 0;
	}

	// Ограничение значения
	if (Clamp != 0 && Output.Size() > Clamp)
		Output *= (Clamp / Output.Size());
}

void UTypes::PControllerGlobalForVector(
	FVector Setpoint,
	FVector Feedback,
	FVector& Output,
	float ProportionalCoeff,
	float DeviationTolerance,
	float Clamp,
	int NumberOfChecks,
	bool& bNearlyEqual,
	int& OutputChecksNumber)
{
	PIDController(
		Setpoint,
		Feedback,
		Output,
		ProportionalCoeff,
		Clamp,
		&bNearlyEqual,
		DeviationTolerance,
		NumberOfChecks,
		&OutputChecksNumber);
}

void UTypes::PIControllerGlobalForVector(
	FVector Setpoint,
	FVector Feedback,
	FVector& Output,
	float DeltaTime,
	float ProportionalCoeff,
	float IntegralCoeff,
	float DeviationTolerance,
	float Clamp,
	int NumberOfChecks,
	bool& bNearlyEqual,
	FVector& IntegralValue,
	int& OutputChecksNumber)
{
	PIDController(
		Setpoint,
		Feedback,
		Output,
		ProportionalCoeff,
		Clamp,
		&bNearlyEqual,
		DeviationTolerance,
		NumberOfChecks,
		&OutputChecksNumber,
		DeltaTime,
		&IntegralValue,
		IntegralCoeff);
}

void UTypes::PIDControllerGlobalForVector(
	FVector Setpoint,
	FVector Feedback,
	FVector& Output,
	float DeltaTime,
	float ProportionalCoeff,
	float IntegralCoeff,
	float DerivativeCoeff,
	float DeviationTolerance,
	float Clamp,
	int NumberOfChecks,
	bool& bNearlyEqual,
	FVector& IntegralValue,
	FVector& PreviousErrorValue,
	int& OutputChecksNumber)
{
	PIDController(
		Setpoint,
		Feedback,
		Output,
		ProportionalCoeff,
		Clamp,
		&bNearlyEqual,
		DeviationTolerance,
		NumberOfChecks,
		&OutputChecksNumber,
		DeltaTime,
		&IntegralValue,
		IntegralCoeff,
		&PreviousErrorValue,
		DerivativeCoeff);
}
// ----------------------------------------------------------------------------------------------------



/* ---   Interface   --- */
void UTypes::AddEffectBySurfaceType(
	AActor* TakeEffectActor,
	FName NameBoneHit,
	TSubclassOf<UOP_StateEffect> AddEffectClass,
	EPhysicalSurface SurfaceType)
{
	if (SurfaceType != EPhysicalSurface::SurfaceType_Default && TakeEffectActor && AddEffectClass)
	{
		UOP_StateEffect* myEffect = Cast<UOP_StateEffect>(AddEffectClass->GetDefaultObject());
		if (myEffect)
		{
			bool bIsHavePossibleSurface = false;
			int8 i = 0;
			while (i < myEffect->PossibleInteractSurface.Num() && !bIsHavePossibleSurface)
			{
				if (myEffect->PossibleInteractSurface[i] == SurfaceType)
				{
					bIsHavePossibleSurface = true;
					bool bIsCanAddEffect = false;
					if (!myEffect->bIsStackable)
					{
						int8 j = 0;
						TArray<UOP_StateEffect*> CurrentEffects;
						IOP_IGameActor* myInterface = Cast<IOP_IGameActor>(TakeEffectActor);
						if (myInterface)
						{
							CurrentEffects = myInterface->GetAllCurrentEffects();
						}

						if (CurrentEffects.Num() > 0)
						{
							while (j < CurrentEffects.Num() && !bIsCanAddEffect)
							{
								if (CurrentEffects[j]->GetClass() != AddEffectClass)
								{
									bIsCanAddEffect = true;
								}
								j++;
							}
						}
						else
						{
							bIsCanAddEffect = true;
						}

					}
					else
					{
						bIsCanAddEffect = true;
					}

					if (bIsCanAddEffect)
					{

						UOP_StateEffect* NewEffect = NewObject<UOP_StateEffect>(TakeEffectActor, AddEffectClass);
						if (NewEffect)
						{
							NewEffect->InitObject(TakeEffectActor, NameBoneHit);
						}
					}

				}
				i++;
			}
		}

	}
}
// ----------------------------------------------------------------------------------------------------



/* ---   Test   --- */
//void UTypes::TestType(
//	int Input,
//	int& Output)
//{
//	Output++;
//
//}
// ----------------------------------------------------------------------------------------------------

// UE_LOG(LogTemp, Warning, TEXT("index is %d"), (int)qwe2);
