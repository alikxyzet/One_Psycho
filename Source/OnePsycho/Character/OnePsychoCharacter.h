﻿// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"

#include "Components/ArrowComponent.h"

#include "OnePsycho/FuncLibrary/Types.h"
#include "OnePsycho/Actor/WeaponDefault.h"
#include "OnePsycho/Game/OnePsychoGameInstance.h"
#include "OnePsychoInventoryComponent.h"
#include "OPCharacterHealthComponent.h"
#include "OnePsycho/Interface/OP_IGameActor.h"
#include "OnePsycho/StateEffects/OP_StateEffect.h"

#include "OnePsychoCharacter.generated.h"

/* ---   Delegate   --- */

// ----------------------------------------------------------------------------------------------------



UCLASS(Blueprintable)
class AOnePsychoCharacter : public ACharacter, public IOP_IGameActor
{
	GENERATED_BODY()

public:
	AOnePsychoCharacter();



	/* ---   Delegate   --- */

	// ----------------------------------------------------------------------------------------------------

	/* ---   Basic   --- */

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Реакции на событие
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// ----------------------------------------------------------------------------------------------------



	/* ---   Inventory   --- */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Inventory", meta = (AllowPrivateAccess = "true"))
		class UOnePsychoInventoryComponent* InventoryComponent;

	void TrySwicthNextWeapon();
	void TrySwitchPreviousWeapon();
	// ----------------------------------------------------------------------------------------------------



	/* ---   Health   --- */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Health", meta = (AllowPrivateAccess = "true"))
		class UOPCharacterHealthComponent* CharHealthComponent = nullptr;

	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

	UFUNCTION()
		void CharDead();

	void EnableRagdoll();

	FTimerHandle TimerHandle_RagDollTimer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim")
		TArray<UAnimMontage*> DeadsAnim;

	/** Реакция на выпадение из мира от UE4 */
	/** Reaction to falling out of the world from UE4 */
	virtual void FellOutOfWorld(const class UDamageType& dmgType) override;
	// ----------------------------------------------------------------------------------------------------



	/* ---   Camera   --- */
	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
		class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Camera", meta = (AllowPrivateAccess = "true"))
		class USpringArmComponent* CameraBoom;

	void OffsetForXY(float OffsetForXY_In, float& TargetOffsetXY);

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera|Offset to cursor")
		float ProportionalCoeffForCamera = 0.2f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera|Offset to cursor")
		float OffsetCoef = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera|Offset to cursor")
		float DeadZone = 0.0f;
	// ----------------------------------------------------------------------------------------------------



	/* ---   Event   --- */
	// 
	UFUNCTION()
		void WeaponReloadStart(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadStart_BP(UAnimMontage* Anim);
	// 
	UFUNCTION()
		void WeaponReloadEnd(bool bIsSuccess, int32 AmmoSafe);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadEnd_BP(bool bIsSuccess);
	// 
	UFUNCTION()
		void WeaponFireStart(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponFireStart_BP(UAnimMontage* Anim);
	//
	UFUNCTION(BlueprintNativeEvent)
		void CharDead_BP();
	// ----------------------------------------------------------------------------------------------------



	/* ---   Cursor   --- */
	// Tick Func
	UFUNCTION()
		void CursorTick();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		FVector CursorSize = FVector(20.f, 40.f, 40.f);

	UDecalComponent* CurrentCursor = nullptr;

	UFUNCTION(BlueprintCallable)
		UDecalComponent* GetCursorToWorld();
	// ----------------------------------------------------------------------------------------------------



	/* ---   Movement   --- */
	// Tick Func
	UFUNCTION()
		void MovementTick();

	// Статус передвижения
	//UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Movement")
	UPROPERTY(Replicated)
		EMovementState MovementState = EMovementState::Run_State;

	// Виды статусов передвижения
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovementSpeedInfo;

	// Состояния команд статуса передвижения
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement|State Control")
		bool bSprintRunEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement|State Control")
		bool bWalkEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement|State Control")
		bool bAimEnabled = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement|State Control")
		bool bIsAlive = true;

	// Шаг изменения ротации
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement|Rotation")
		float RotationChangeStep = 5;
	// Коррекция ротации в плоскости XY
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement|Rotation")
		float RotationShift = 0;

	// Реализация выносливости
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement|Stamina")
		float StaminaStepDown = 0.5;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement|Stamina")
		float StaminaStepUp = 0.5;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement|Stamina")
		float BufferSprintRunStamina = 400;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement|Stamina")
		float SprintRunStamina = MovementSpeedInfo.SprintRunSpeedRun;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement|Stamina")
		float SprintRunStaminaUpperLimit = MovementSpeedInfo.SprintRunSpeedRun + BufferSprintRunStamina;

	// Текущая скорость
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement|Stamina")
		float ResSpeed = MovementSpeedInfo.RunSpeedNormal;

	float AxisX = 0.0f;
	float AxisY = 0.0f;

	// Получение направления движения по X
	UFUNCTION()
		void InputAxisX(float Value);
	// Получение направления движения по Y
	UFUNCTION()
		void InputAxisY(float Value);

	// Изменение статуса передвижения
	UFUNCTION(BlueprintCallable)
		void ChangeMovementState();

	// Изменение скорости передвижения
	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();

	// Получениие состояния жизни
	UFUNCTION(BlueprintCallable, BlueprintPure)
		bool GetIsAlive();

	UFUNCTION(BlueprintCallable, BlueprintPure)
		EMovementState GetMovementState();

	UFUNCTION(Server, Unreliable)
		void SetActorRotationByYaw_OnServer(float Yaw);

	UFUNCTION(NetMulticast, Unreliable)
		void SetActorRotationByYaw_Multicast(float Yaw);

	UFUNCTION(Server, Reliable)
		void SetMovementState_OnServer(EMovementState NewState);

	UFUNCTION(NetMulticast, Reliable)
		void SetMovementState_Multicast(EMovementState NewState);
	// ----------------------------------------------------------------------------------------------------



	/* ---   Weapon   --- */
	UPROPERTY(Replicated)
		AWeaponDefault* CurrentWeapon = nullptr;

	// Чтение текущего оружия
	UFUNCTION(BlueprintCallable, BlueprintPure)
		AWeaponDefault* GetCurrentWeapon();
	// Инициализация оружия
	UFUNCTION(BlueprintCallable)
		void InitWeapon(EWeaponType IdWeapon, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);
	// Перезарядка оружия
	UFUNCTION(BlueprintCallable)
		void TryReloadWeapon();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapons")
		EWeaponType Enum_WeaponName = EWeaponType::Rifle_v1;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo")
	FName InitWeaponName = EWeaponName[(uint8)Enum_WeaponName];

	UFUNCTION(BlueprintCallable, BlueprintPure)
		int32 GetCurrentWeaponIndex();
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
		int32 CurrentIndexWeapon = 0;
	// ----------------------------------------------------------------------------------------------------



	/* ---   Attack   --- */
	UFUNCTION()
		void InputAttackPressed();
	UFUNCTION()
		void InputAttackReleaset();

	UFUNCTION(BlueprintCallable)
		void AttackCharEvent(bool bIsFiring);
	// ----------------------------------------------------------------------------------------------------



	/* ---   Aiming   --- */
	UFUNCTION()
		void InputAimingPressed();
	UFUNCTION()
		void InputAimingReleaset();

	UFUNCTION(BlueprintCallable)
		void UpdateAimingStatus();
	// ----------------------------------------------------------------------------------------------------



	/* ---   Interface   --- */
	TArray<UOP_StateEffect*> Effects;

	EPhysicalSurface GetSurfuceType() override;
	TArray<UOP_StateEffect*> GetAllCurrentEffects() override;
	void RemoveEffect(UOP_StateEffect* RemoveEffect) override;
	void AddEffect(UOP_StateEffect* newEffect) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
		TSubclassOf<UOP_StateEffect> AbilityEffect;

	// Ability function
	void TryAbilityEnabled();
	// ----------------------------------------------------------------------------------------------------



	/* ---   ???   --- */

	// ----------------------------------------------------------------------------------------------------



	/* ---   For Demo   --- */


		//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo")
		//	TArray<FName> ArrDataTable;// = GIDemo->WeaponInfoTable->GetRowNames();
			//ArrDataTable = myGI->WeaponInfoTable->GetRowNames();
	int TestDropMesh = 0;
	// ----------------------------------------------------------------------------------------------------



};

