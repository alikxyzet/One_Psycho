// Fill out your copyright notice in the Description page of Project Settings.


#include "OPCharacterHealthComponent.h"

void UOPCharacterHealthComponent::ChangeHealthValue(float ChangeValue)
{
	float CurrentDamage = ChangeValue * CoefDamage;

	if (Shield > 0.0f && ChangeValue < 0.0f)
	{
		ChangeShieldValue(ChangeValue);

		//if (Shield < 0.0f)
		//{
		//	//FX
		//	//UE_LOG(LogTemp, Warning, TEXT("UOPCharacterHealthComponent::ChangeHealthValue - Sheild < 0"));
		//}

		// TEST LOG
		//UE_LOG(LogTemp, Warning, TEXT("UOPCharacterHealthComponent::ChangeHealthValue - Shield > 0.0f && ChangeValue < 0.0f"))
	}
	else
	{
		Super::ChangeHealthValue(ChangeValue);

		// TEST LOG
		//UE_LOG(LogTemp, Warning, TEXT("UOPCharacterHealthComponent::ChangeHealthValue - Shield < 0.0f || ChangeValue > 0.0f"))
	}
}

float UOPCharacterHealthComponent::GetCurrentShield()
{
	return Shield;
}

void UOPCharacterHealthComponent::ChangeShieldValue(float ChangeValue)
{
	Shield += ChangeValue;

	if (Shield > 100.0f)
	{
		Shield = 100.0f;
	}
	else
	{
		if (Shield < 0.0f)
			Shield = 0.0f;
	}

	OnShieldChange.Broadcast(Shield, ChangeValue);

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CollDownShieldTimer, this, &UOPCharacterHealthComponent::CoolDownShieldEnd, CoolDownShieldRecoverTime, false);

		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
	}
}

void UOPCharacterHealthComponent::CoolDownShieldEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoveryRateTimer, this, &UOPCharacterHealthComponent::RecoveryShield, ShieldRecoverRate, true);
	}
}

void UOPCharacterHealthComponent::RecoveryShield()
{
	float tmp = Shield;
	tmp = tmp + ShieldRecoverValue;
	if (tmp > 100.0f)
	{
		Shield = 100.0f;
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
		}
	}
	else
	{
		Shield = tmp;
	}

	OnShieldChange.Broadcast(Shield, ShieldRecoverValue);
}

float UOPCharacterHealthComponent::GetShieldValue()
{
	return Shield;
}
