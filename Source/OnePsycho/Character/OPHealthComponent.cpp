// Fill out your copyright notice in the Description page of Project Settings.


#include "OPHealthComponent.h"

// Sets default values for this component's properties
UOPHealthComponent::UOPHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UOPHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UOPHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

float UOPHealthComponent::GetCurrentHealth()
{
	return Health;
}

void UOPHealthComponent::SetCurrentHealth(float NewHealth)
{
	Health = NewHealth;
}

bool UOPHealthComponent::GetIsAlive()
{
	return bIsAlive;
}

void UOPHealthComponent::ChangeHealthValue(float ChangeValue)
{
	ChangeValue = ChangeValue * CoefDamage;

	Health += ChangeValue;

	OnHealthChange.Broadcast(Health, ChangeValue);

	if (Health > 100.0f)
	{
		Health = 100.0f;
	}
	else
	{
		if (Health < 0.0f)
		{
			OnDead.Broadcast();
			bIsAlive = false;
		}
	}
}