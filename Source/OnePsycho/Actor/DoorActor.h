﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "OnePsycho/FuncLibrary/Types.h"

#include "DoorActor.generated.h"



UCLASS()
class ONEPSYCHO_API ADoorActor : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	ADoorActor();


	/* ---   Basic   --- */

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Шаг времени цикла
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Door")
		float TimerStep = 0.01f;
	// ----------------------------------------------------------------------------------------------------



	/* ---   Door State   --- */
	// Флаг игнора изменения статуса двери
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Door")
		bool bIgnoreGlobalControls;

	// Фиксированный статус двери
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Door")
		EDoorState DoorStateSelection = EDoorState::OpenClose_State;

	// Текущий статус двери
	UPROPERTY()
		EDoorState DoorState;

	// BP функция новых статуса и положения двери
	UFUNCTION(BlueprintCallable)
		void NewState(EDoorState NewDoorState, bool bReturn);
	// ----------------------------------------------------------------------------------------------------



	/* ---   Doors control   --- */
	// Флаг действия двери
	UPROPERTY(BlueprintReadWrite)
		bool bDoorAction;

	// BP функция открытия двери (с проверкой)
	UFUNCTION(BlueprintCallable)
		void OpenDoor();

	// BP функция закрытия двери (с проверкой)
	UFUNCTION(BlueprintCallable)
		void CloseDoor();

	// BP функция изменения состояния двери (с проверкой)
	UFUNCTION(BlueprintCallable)
		void OpenCloseDoor();

	// BP EVENT для запуска действия двери (открытие или закрытие)
	UFUNCTION(BlueprintImplementableEvent)
		void DoorActionStart();
	// ----------------------------------------------------------------------------------------------------



	// Время сброса защиты от многократного действия
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Door")
		float ResetTime_DoOne = 0.5f;
private:

	// Приватные элементы таймера для защиты от многократного действия
	bool bDoOne = true;
	FTimerHandle Handle;
	void DoOneReset();
};
