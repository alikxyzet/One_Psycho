﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "DoorActor.h"

#include "Kismet/KismetSystemLibrary.h"

// Sets default values
ADoorActor::ADoorActor()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}



/* ---   Basic   --- */

// Called when the game starts or when spawned
void ADoorActor::BeginPlay()
{
	Super::BeginPlay();

	// Начальные статус и положение двери
	DoorState = DoorStateSelection;

	if (DoorState == EDoorState::IsOpen_State || DoorState == EDoorState::OnlyClose_State)
		bDoorAction = true;
	else
		bDoorAction = false;

	DoorActionStart();
	//---//
}

// Called every frame
void ADoorActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}
// ----------------------------------------------------------------------------------------------------


/* ---   Door State   --- */

void ADoorActor::NewState(EDoorState NewDoorState, bool bReturn)
{
	if (bReturn || bIgnoreGlobalControls)
		DoorState = DoorStateSelection;
	else
		DoorState = NewDoorState;

	if (DoorState == EDoorState::IsOpen_State || DoorState == EDoorState::OnlyClose_State)
		bDoorAction = true;
	else
		bDoorAction = false;

	DoorActionStart();
}
// ----------------------------------------------------------------------------------------------------



/* ---   Doors control   --- */

void ADoorActor::OpenDoor()
{
	if (DoorState == EDoorState::OnlyOpen_State || DoorState == EDoorState::OpenClose_State)
	{
		bDoorAction = true;
		DoorActionStart();
	}
}

void ADoorActor::CloseDoor()
{
	if (DoorState == EDoorState::OnlyClose_State || DoorState == EDoorState::OpenClose_State)
	{
		bDoorAction = false;
		DoorActionStart();
	}
}

void ADoorActor::OpenCloseDoor()
{
	if (bDoOne)
	{
		if (bDoorAction)
			CloseDoor();
		else
			OpenDoor();

		bDoOne = false;
	}
	// Таймер для защиты от многократного действия
	GetWorld()->GetTimerManager().SetTimer(Handle, this, &ADoorActor::DoOneReset, ResetTime_DoOne, false);
}
// ----------------------------------------------------------------------------------------------------



void ADoorActor::DoOneReset()
{
	bDoOne = true;
}