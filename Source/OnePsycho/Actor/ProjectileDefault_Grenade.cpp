﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectileDefault_Grenade.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"	// Отрисовка для отладки
#include "Net/UnrealNetwork.h"

bool bShowDebugGranade = false;
FAutoConsoleVariableRef CVARExplodeShow(
	TEXT("OP.Granade.DebugExplode"),
	bShowDebugGranade,
	TEXT("Draw Debug for Explode"),
	ECVF_Cheat);

// Called when the game starts or when spawned
void AProjectileDefault_Grenade::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AProjectileDefault_Grenade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	TimerExplosion(DeltaTime);
}



// Ограничение времени существования текущего объекта
void AProjectileDefault_Grenade::LifeSpan(FProjectileInfo InitParam)
{
	this->SetLifeSpan(InitParam.ProjectileLifeTime + 10);
}



void AProjectileDefault_Grenade::TimerExplosion(float DeltaTime)
{
	if (bTimerEnabled)
	{
		if (TimerToExplosion > ProjectileSetting.ProjectileLifeTime)
		{
			// Explosion
			Explosion();
		}
		else
		{
			TimerToExplosion += DeltaTime;
		}
	}
}

void AProjectileDefault_Grenade::BulletCollisionSphereHit(
	UPrimitiveComponent* HitComp,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	FVector NormalImpulse,
	const FHitResult& Hit)
{
	if (!bTimerEnabled)
	{
		Explosion();
	}

	Super::BulletCollisionSphereHit(
		HitComp,
		OtherActor,
		OtherComp,
		NormalImpulse,
		Hit);
}

void AProjectileDefault_Grenade::ImpactProjectile()
{
	// Init Grenade
	bTimerEnabled = true;
}

void AProjectileDefault_Grenade::Explosion()
{
	// Отрисовка радиусов взрыва
	if (bShowDebugGranade)
	{
		DrawDebugSphere(
			GetWorld(),
			GetActorLocation(),
			ProjectileSetting.ProjectileMinRadiusDamage,
			12,
			FColor::Red,
			false,
			12.0f);

		float AverageRadius = (ProjectileSetting.ProjectileMaxRadiusDamage + ProjectileSetting.ProjectileMinRadiusDamage) / 2;
		DrawDebugSphere(
			GetWorld(),
			GetActorLocation(),
			AverageRadius,
			12,
			FColor::Orange,
			false,
			12.0f);

		DrawDebugSphere(
			GetWorld(),
			GetActorLocation(),
			ProjectileSetting.ProjectileMaxRadiusDamage,
			12,
			FColor::Yellow,
			false,
			12.0f);
	}


	bTimerEnabled = false;

	// Использование визуального эффекта
	if (ProjectileSetting.ExplodeFX)
	{
		SpawnFX(
			ProjectileSetting.ExplodeFX,
			FTransform(
				GetActorRotation(),
				GetActorLocation(),
				FVector(1.0f)));
	}

	// Использование звукового эффекта
	if (ProjectileSetting.ExplodeSound)
	{
		SpawnSound(
			ProjectileSetting.ExplodeSound,
			GetActorLocation());
	}


	TArray<AActor*> IgnoredActor;
	RadialDamage_Multicast(IgnoredActor);

	this->Destroy();
}

void AProjectileDefault_Grenade::RadialDamage_Multicast_Implementation(const TArray<AActor*>& IgnoredActor)
{
	UGameplayStatics::ApplyRadialDamageWithFalloff(
		GetWorld(),
		ProjectileSetting.ExplodeMaxDamage,
		ProjectileSetting.ExplodeMaxDamage * 0.2f,
		GetActorLocation(),
		ProjectileSetting.ProjectileMinRadiusDamage,
		ProjectileSetting.ProjectileMaxRadiusDamage,
		5,
		NULL,
		IgnoredActor,
		this,
		nullptr);
}
