﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"

#include "ProjectileDefault.h"
#include "OnePsycho/FuncLibrary/Types.h"
#include "OnePsycho/Character/OnePsychoInventoryComponent.h"

#include "DrawDebugHelpers.h"	// Рисовалка для отладки
#include "Kismet/GameplayStatics.h"

#include "WeaponDefault.generated.h"

/* ---   Delegate   --- */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadStart, UAnimMontage*, Anim);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponFireStart, UAnimMontage*, Anim);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponReloadEnd, bool, bIsSuccess, int32, AmmoSafe);
// ----------------------------------------------------------------------------------------------------



UCLASS()
class ONEPSYCHO_API AWeaponDefault : public AActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AWeaponDefault();



	/* ---   Delegate   --- */
	FOnWeaponReloadStart OnWeaponReloadStart;
	FOnWeaponReloadEnd OnWeaponReloadEnd;
	FOnWeaponFireStart OnWeaponFireStart;
	// ----------------------------------------------------------------------------------------------------



	/* ---   Basic   --- */

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UStaticMeshComponent* StaticMeshWeapon = nullptr;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UArrowComponent* ShootLocation = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UArrowComponent* ClipDropLocation = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
	class UArrowComponent* SleeveBulletDropLocation = nullptr;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	// ----------------------------------------------------------------------------------------------------

private:



public:

	bool bWeaponAiming = false;

	/* ---   Event   --- */
// 
	UFUNCTION()
	void WeaponReloadStart(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadStart_BP(UAnimMontage* Anim);
	// 
	UFUNCTION()
	void WeaponReloadEnd();
	UFUNCTION(BlueprintNativeEvent)
	void WeaponReloadEnd_BP();
	// 
	UFUNCTION()
	void WeaponFireStart(UAnimMontage* Anim);
	UFUNCTION(BlueprintNativeEvent)
	void WeaponFireStart_BP(UAnimMontage* Anim);
	// ----------------------------------------------------------------------------------------------------



	/* ---   Weapon Info   --- */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
	FWeaponInfo WeaponSetting;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
	FAdditionalWeaponInfo WeaponInfo;

	void DestroyUnusedComponents();
	void WeaponInit();

	UFUNCTION(Server, Reliable)
	void UpdateStateWeapon_OnServer(EMovementState NewMovementState);

	void ChangeDispersion();

	UFUNCTION(BlueprintCallable)
	int32 GetWeaponRound() const;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = "Weapon Info")
	FAdditionalWeaponInfo AdditionalWeaponInfo;
	// ----------------------------------------------------------------------------------------------------



	/* ---   Projectile   --- */
	FProjectileInfo GetProjectile() const;
	// ----------------------------------------------------------------------------------------------------



	/* ---   Fire   --- */
	// Tick Func
	void FireTick(float DeltaTime);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	bool bWeaponFiring = false;

	bool BlockFire = false;

	float FireTimer = 0.f;

	UPROPERTY(Replicated)
	FVector ShootEndLocation = FVector(0);

	bool ChecWeaponCanFire() const;
	FVector GetFireEndLocation() const;
	int8 GetNumberProjectileByShot() const;

	UFUNCTION(Server, Reliable, BlueprintCallable)
	void SetWeaponStateFire_OnServer(bool bIsFire);

	UFUNCTION(BlueprintCallable)
	void SetWeaponStateAimed(bool bIsAimed);

	void Fire();

	void ReactionToHit(
		UPrimitiveComponent* OtherComp,
		const FHitResult& Hit,
		const FProjectileInfo& ProjectileSetting);
	// ----------------------------------------------------------------------------------------------------



	/* ---   Reload   --- */
	// Tick Func
	void ReloadTick(float DeltaTime);

	// В режиме перезарядки
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
	bool bWeaponReloading = false;

	//Время перезарядки
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
	float ReloadTimer = 0.f;

	// Запуск процесса перезарядки
	void InitReload();

	// Завершение перезарядки
	void FinishReload();

	bool CheckCanWeaponReload();
	int8 GetAviableAmmoForReload();

	// ----------------------------------------------------------------------------------------------------



	/* ---   Dispersion   --- */
	// Tick Func
	void DispersionTick(float DeltaTime);

	// Флаг передвижения персонажа
	//UPROPERTY(Replicated)
	bool bShouldReduceDispersion = false;

	float CurrentDispersion = 0.f;
	float CurrentDispersionMin = 1.f;
	float CurrentDispersionMax = 0.1f;
	float CurrentDispersionRecoil = 0.1f;
	float CurrentDispersionReduction = 0.1f;

	void ChangeDispersionByShot();

	float GetCurrentDispersion() const;

	FVector ApplyDispersionToShoot(FVector DirectionShoot) const;

	// ----------------------------------------------------------------------------------------------------



	/* ---   Drop Mesh   --- */
	void InitDropMesh(FDropMeshInfo& DropMeshInfo, UArrowComponent& ArrowComponent);

	//UFUNCTION(Server, Reliable)
	//void InitDropMesh_OnServer(FDropMeshInfo& DropMeshInfo, UArrowComponent& ArrowComponent);
	// ----------------------------------------------------------------------------------------------------



	/* ---   Inventory   --- */
	void CancelReload();
	// ----------------------------------------------------------------------------------------------------



	/* ---   NetWork   --- */
	UFUNCTION(Server, Unreliable)
	void UpdateWeaponByCharacterMovementState_OnServer(FVector NewShootEndLocation, bool bNewShouldReduceDispersion);

	UFUNCTION(NetMulticast, Unreliable)
	void AnimWeaponStart_Multicast(UAnimMontage* Anim);

	UFUNCTION(NetMulticast, Unreliable)
	void ShellDropFire_Multicast(
		UStaticMesh* DropMesh,
		FTransform Offset,
		FVector DropImpulseDirection,
		float LifeTimeMesh);

	UFUNCTION(NetMulticast, Unreliable)
	void SpawnSoundAndFX_Multicast(
		UParticleSystem* in_FX,
		USoundBase* in_Sound,
		const FTransform& in_Transform);

	UFUNCTION(NetMulticast, Reliable)
	void SpawnHitDecal_Multicast(UMaterialInterface* DecalMaterial, UPrimitiveComponent* OtherComp, FHitResult HitResult);
	// ----------------------------------------------------------------------------------------------------



	// Remove !!! Debug
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug|ReloadLogic")
	float ReloadTime = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug|Lines")
	bool bShowDebug2 = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug|Lines")
	float SizeVectorToChangeShootDirectionLogic = 100.f;

	// ----------------------------------------------------------------------------------------------------
};