﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "ProjectileDefault.h"
#include "ProjectileDefault_Grenade.generated.h"

/**
 *
 */
UCLASS()
class ONEPSYCHO_API AProjectileDefault_Grenade : public AProjectileDefault
{
	GENERATED_BODY()

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual	void LifeSpan(FProjectileInfo InitParam) override;

	void TimerExplosion(float DeltaTime);

	virtual	void BulletCollisionSphereHit(
		class UPrimitiveComponent* HitComp,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		FVector NormalImpulse,
		const FHitResult& Hit) override;

	virtual	void ImpactProjectile() override;

	void Explosion();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grenade")
	bool bTimerEnabled = false;

	float TimerToExplosion = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Grenade")
	float TimeToExplosion = 5.f;

	UFUNCTION(NetMulticast, Reliable)
	void RadialDamage_Multicast(const TArray<AActor*>& IgnoredActor);
};
