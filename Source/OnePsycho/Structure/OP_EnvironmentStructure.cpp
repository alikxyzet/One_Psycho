// Fill out your copyright notice in the Description page of Project Settings.


#include "OP_EnvironmentStructure.h"

#include "Materials/MaterialInterface.h"
#include "PhysicalMaterials/PhysicalMaterial.h"

// Sets default values
AOP_EnvironmentStructure::AOP_EnvironmentStructure()
{
	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AOP_EnvironmentStructure::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void AOP_EnvironmentStructure::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

EPhysicalSurface AOP_EnvironmentStructure::GetSurfuceType()
{
	EPhysicalSurface Result = EPhysicalSurface::SurfaceType_Default;
	UStaticMeshComponent* myMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	if (myMesh)
	{
		UMaterialInterface* myMaterial = myMesh->GetMaterial(0);
		if (myMaterial)
		{
			Result = myMaterial->GetPhysicalMaterial()->SurfaceType;
		}
	}
	return Result;
}
TArray<UOP_StateEffect*> AOP_EnvironmentStructure::GetAllCurrentEffects()
{
	return Effects;
}

void AOP_EnvironmentStructure::RemoveEffect(UOP_StateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
}

void AOP_EnvironmentStructure::AddEffect(UOP_StateEffect* newEffect)
{
	Effects.Add(newEffect);
}