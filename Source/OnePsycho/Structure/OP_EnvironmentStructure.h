// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "OnePsycho/Interface/OP_IGameActor.h"
#include "OnePsycho/StateEffects/OP_StateEffect.h"

#include "OP_EnvironmentStructure.generated.h"

UCLASS()
class ONEPSYCHO_API AOP_EnvironmentStructure : public AActor, public IOP_IGameActor
{
	GENERATED_BODY()

public:
	// Sets default values for this actor's properties
	AOP_EnvironmentStructure();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	EPhysicalSurface GetSurfuceType() override;

	TArray<UOP_StateEffect*> GetAllCurrentEffects() override;
	void RemoveEffect(UOP_StateEffect* RemoveEffect)override;
	void AddEffect(UOP_StateEffect* newEffect)override;

	//Effect
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
		TArray<UOP_StateEffect*> Effects;
};
