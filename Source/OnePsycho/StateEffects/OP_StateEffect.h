﻿// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"

#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Particles/ParticleSystemComponent.h"
#include "Components/MeshComponent.h"
#include "Components/ShapeComponent.h"
#include "PhysicsEngine/BodyInstance.h"

#include "OP_StateEffect.generated.h"

/**
 *
 */
UCLASS(Blueprintable, BlueprintType)
class ONEPSYCHO_API UOP_StateEffect : public UObject
{
	GENERATED_BODY()

public:

	virtual bool InitObject(AActor* Actor, FName NameBoneHit);
	virtual void DestroyObject();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
		TArray<TEnumAsByte<EPhysicalSurface>> PossibleInteractSurface;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
		bool bIsStackable = false;

	AActor* myActor = nullptr;
};



/* ---   Execute Once   --- */
UCLASS()
class ONEPSYCHO_API UOP_StateEffect_ExecuteOnce : public UOP_StateEffect
{
	GENERATED_BODY()

public:
	bool InitObject(AActor* Actor, FName NameBoneHit) override;
	void DestroyObject() override;

	virtual void ExecuteOnce();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Once")
		float Power = 20.0f;
};
// ----------------------------------------------------------------------------------------------------



/* ---   Execute Timer   --- */
UCLASS()
class ONEPSYCHO_API UOP_StateEffect_ExecuteTimer : public UOP_StateEffect
{
	GENERATED_BODY()

public:
	bool InitObject(AActor* Actor, FName NameBoneHit) override;
	void DestroyObject() override;

	virtual void Execute();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
		float Power = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
		float Timer = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
		float RateTime = 1.0f;

	FTimerHandle TimerHandle_ExecuteTimer;
	FTimerHandle TimerHandle_EffectTimer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
		UParticleSystem* ParticleEffect = nullptr;

	UParticleSystemComponent* ParticleEmitter = nullptr;
};
// ----------------------------------------------------------------------------------------------------



/* ---   Transmitted by Touch   --- */
UCLASS()
class ONEPSYCHO_API UOP_StateEffect_TransmittedByTouch : public UOP_StateEffect_ExecuteTimer
{
	GENERATED_BODY()

public:
	bool InitObject(AActor* Actor, FName NameBoneHit) override;
	void DestroyObject() override;

	//void Execute() override;

	// Передаваемый эффект
	// Transmitted effect
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting TransmittedByTouch")
		TSubclassOf<UOP_StateEffect> TransmittedEffect = nullptr;

	UFUNCTION()
		void FOnActorHit(
			AActor* HitActor,
			AActor* OtherActor,
			FVector NormalImpulse,
			const FHitResult& Hit);
};
// ----------------------------------------------------------------------------------------------------
