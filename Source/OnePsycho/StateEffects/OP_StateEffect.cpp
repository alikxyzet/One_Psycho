﻿// Fill out your copyright notice in the Description page of Project Settings.


#include "OP_StateEffect.h"

#include "OnePsycho/Character/OPHealthComponent.h"
#include "OnePsycho/Interface/OP_IGameActor.h"
#include "Kismet/GameplayStatics.h"

#include "OnePsycho/FuncLibrary/Types.h"

bool UOP_StateEffect::InitObject(AActor* Actor, FName NameBoneHit)
{

	myActor = Actor;

	IOP_IGameActor* myInterface = Cast<IOP_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->AddEffect(this);
	}

	return true;
}

void UOP_StateEffect::DestroyObject()
{
	IOP_IGameActor* myInterface = Cast<IOP_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->RemoveEffect(this);
	}

	myActor = nullptr;
	if (this && this->IsValidLowLevel())
	{
		this->ConditionalBeginDestroy();
	}
}



/* ---   Execute Once   --- */
bool UOP_StateEffect_ExecuteOnce::InitObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);
	ExecuteOnce();
	return true;
}

void UOP_StateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UOP_StateEffect_ExecuteOnce::ExecuteOnce()
{
	if (myActor)
	{
		UOPHealthComponent* myHealthComp = Cast<UOPHealthComponent>(myActor->GetComponentByClass(UOPHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue(Power);
		}
	}

	DestroyObject();
}
// ----------------------------------------------------------------------------------------------------



/* ---   Execute Timer   --- */
bool UOP_StateEffect_ExecuteTimer::InitObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UOP_StateEffect_ExecuteTimer::DestroyObject, Timer, false);
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UOP_StateEffect_ExecuteTimer::Execute, RateTime, true);
	}

	if (ParticleEffect)
	{
		FName NameBoneToAttached = NameBoneHit;
		FVector Loc = FVector(0);

		USceneComponent* myMesh = Cast<USceneComponent>(myActor->GetComponentByClass(USkeletalMeshComponent::StaticClass()));
		if (myMesh)
		{
			ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(
				ParticleEffect,
				myMesh,
				NameBoneToAttached,
				Loc,
				FRotator::ZeroRotator,
				EAttachLocation::SnapToTarget,
				false);
		}
		else
		{
			ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(
				ParticleEffect, 
				myActor->GetRootComponent(), 
				NameBoneToAttached, 
				Loc, 
				FRotator::ZeroRotator, 
				EAttachLocation::SnapToTarget, 
				false);
		}
	}

	return true;
}

void UOP_StateEffect_ExecuteTimer::DestroyObject()
{
	ParticleEmitter->DestroyComponent();
	ParticleEmitter = nullptr;
	Super::DestroyObject();
}

void UOP_StateEffect_ExecuteTimer::Execute()
{
	if (myActor)
	{
		UOPHealthComponent* myHealthComp = Cast<UOPHealthComponent>(myActor->GetComponentByClass(UOPHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue(Power);
		}
	}
}
// ----------------------------------------------------------------------------------------------------



/* ---   Transmitted by Touch   --- */
bool UOP_StateEffect_TransmittedByTouch::InitObject(AActor* Actor, FName NameBoneHit)
{
	Super::InitObject(Actor, NameBoneHit);

	// Привязать делегат попадания актора
	myActor->OnActorHit.AddDynamic(this, &UOP_StateEffect_TransmittedByTouch::FOnActorHit);

	return true;
}

void UOP_StateEffect_TransmittedByTouch::DestroyObject()
{
	// Отвязать делегат попадания актора
	myActor->OnActorHit.RemoveDynamic(this, &UOP_StateEffect_TransmittedByTouch::FOnActorHit);

	Super::DestroyObject();
}

/*
void UOP_StateEffect_TransmittedByTouch::Execute()
{
	Super::Execute();
}
*/

void UOP_StateEffect_TransmittedByTouch::FOnActorHit(
	AActor* HitActor,
	AActor* OtherActor,
	FVector NormalImpulse,
	const FHitResult& Hit)
{
	TArray<UMeshComponent*> OtherMesh;
	OtherActor->GetComponents<UMeshComponent>(OtherMesh);

	int8 i = 0;
	if (OtherMesh.IsValidIndex(0))
	{
		while (i < OtherMesh.Num())
		{
			// Получение информации о SurfaceType соприкасаемого актора
			EPhysicalSurface SurfaceTypeOfOtherActor = OtherMesh[i]->BodyInstance.GetSimplePhysicalMaterial()->SurfaceType;

			UTypes::AddEffectBySurfaceType(OtherActor, Hit.BoneName, TransmittedEffect, SurfaceTypeOfOtherActor);

			//UE_LOG(LogTemp, Warning, TEXT("OtherActor -> Surface Type Index is %d"), (int)SurfaceTypeOfOtherActor);
			i++;
		}
	}
};
// ----------------------------------------------------------------------------------------------------
